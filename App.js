import React from 'react'
import HomeStack from '../Organic/src/navigation/MainStackNavigator'

const App = () => {
  console.disableYellowBox = true;
  return <HomeStack />
};

export default App;

