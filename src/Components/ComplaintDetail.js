import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';
import { scaleHeight, scaleWidth, normalizeFont } from "../Utilities/Responsive";

const { width } = Dimensions.get('window')

export default class ComplaintDetail extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    saveAction = (buttonType) => {
        const { navigation } = this.props;
        const complaintDetail = navigation.getParam('complaintDetail');
        RestClient.get_New_Post("api/vendor/complains/update", { complain_id: complaintDetail.complain_id, status: buttonType }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigation } = this.props;
        const complaintDetail = navigation.getParam('complaintDetail');
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 20 }}>

                        <Text style={[styles.productBox, { marginTop: 25, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }]}>
                            {complaintDetail.title}
                        </Text>
                        <Text
                            multiline={true}
                            style={[styles.productBox, { height: scaleHeight(160), marginTop: 5, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }]}>
                            {complaintDetail.description}
                        </Text>

                        <View style={[styles.rowContainerStyle, { margin: 10 }]}>
                            <TouchableOpacity onPress={() => this.saveAction('1')} style={[styles.goToSignInOuter, { backgroundColor: Constants.Colors.Orange }]}>
                                <Text style={[styles.goToSignInInner]}>Resolved</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.saveAction('0')} style={[styles.goToSignInOuter, { backgroundColor: Constants.Colors.Orange }]}>
                                <Text style={[styles.goToSignInInner]}>Pending</Text>
                            </TouchableOpacity>
                        </View>

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
    productBox: {
        // alignSelf: 'center',
        //alignItems: "center",
        //height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
})