import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator, createAppContainer, } from 'react-navigation';
import Login from '../components/Login'
import SignIn from '../components/SignIn'
import Home from '../components/Home'
import AdjustDeliveryOption from '../components/AdjustDeliveryOption'
import ManageProducts from '../components/ManageProducts'
import ComplaintDetail from '../components/ComplaintDetail'
import Complaints from '../components/Complaints'
import ManageOrders from '../components/ManageOrders'
import NewOrder from '../components/NewOrder'
import NewOrderDetail from '../components/NewOrderDetail'
import OngoingOrder from '../components/OngoingOrder'
import OngoingOrderDetail from '../components/OngoingOrderDetail'
import OngoingOrderList from '../components/OngoingOrderList'
import PreviousOrder from '../components/PreviousOrder'
import SalesAndFigures from '../components/SalesAndFigures'
import DailyTotalSale from '../components/DailyTotalSale'
import MonthlyTotalSale from '../components/MonthlyTotalSale'
import AmountReceived from '../components/AmountReceived'
import MonthTableChart from '../components/MonthTableChart'
import MonthTableChartDetail from '../components/MonthTableChartDetail'
import AddNewDriver from '../components/AddNewDriver'
import SplashScreen from '../components/SplashScreen'
import MyProfile from '../components/MyProfile'

const HomeStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  SignIn: { screen: SignIn },
  Home: { screen: Home },
  MyProfile: { screen: MyProfile },
  AdjustDeliveryOption: { screen: AdjustDeliveryOption },
  ManageProducts: { screen: ManageProducts },
  ComplaintDetail: { screen: ComplaintDetail },
  Complaints: { screen: Complaints },
  ManageOrders: { screen: ManageOrders },
  NewOrder: { screen: NewOrder },
  NewOrderDetail: { screen: NewOrderDetail },
  OngoingOrder: { screen: OngoingOrder },
  OngoingOrderDetail: { screen: OngoingOrderDetail },
  OngoingOrderList: { screen: OngoingOrderList },
  PreviousOrder: { screen: PreviousOrder },
  SalesAndFigures: { screen: SalesAndFigures },
  DailyTotalSale: { screen: DailyTotalSale },
  MonthlyTotalSale: { screen: MonthlyTotalSale },
  AmountReceived: { screen: AmountReceived },
  MonthTableChart: { screen: MonthTableChart },
  MonthTableChartDetail: { screen: MonthTableChartDetail },
  AddNewDriver: { screen: AddNewDriver },
  initialRouteName: "Login"
})

const MySwitchNavigator = createSwitchNavigator({
  Home: HomeStack,
  initialRouteName: HomeStack
});

export default createAppContainer(MySwitchNavigator);