import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';

const { width } = Dimensions.get('window')

export default class SalesAndFigures extends Component {
    constructor(props) {
        super()
        this.state = {
            dailySale: '',
            tokenValue: '',
            productsSoldList: [],
            vendorRatings: 0
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/ratings/show", {}, tokenValue).then((result) => {
                console.log("vendor new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ vendorRatings: Number(result.vendor_ratings) })
                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>
                        <View style={styles.logo} />

                        <SubmitButton
                            onPress={() => navigate("DailyTotalSale")}
                            text="Daily Total Sale"
                        />
                        <SubmitButton
                            onPress={() => navigate("MonthlyTotalSale")}
                            text="Monthly Total Sale"
                        />
                        <SubmitButton
                            onPress={() => navigate("AmountReceived")}
                            text="Amount Received"
                        />
                        {this.state.vendorRatings == 0 ?
                            <SubmitButton
                                text={"Rating"}
                            />
                            :
                            <SubmitButton
                                text={"Rating " + (this.state.vendorRatings).toFixed(2)}
                            />
                        }


                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
})