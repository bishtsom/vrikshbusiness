import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';

const { width } = Dimensions.get('window')

export default class ManageOrders extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            email: '',
            contact: '',
            outletName: '',
            outletAddress: ''
        }
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>
                        <View style={styles.logo} />

                        <SubmitButton
                            //onSubmit={Keyboard.dismiss}
                            onPress={() => navigate("NewOrder")}
                            text="New Order"
                        />
                        <SubmitButton
                            //onSubmit={Keyboard.dismiss}
                            onPress={() => navigate("OngoingOrder")}
                            text="Ongoing Order"
                        />
                        <SubmitButton
                            //onSubmit={Keyboard.dismiss}
                            onPress={() => navigate("PreviousOrder")}
                            text="Previous Order"
                        />

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
})