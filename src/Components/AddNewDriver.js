import React, { Component } from 'react'
import { Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../Utilities/Regex';
import _ from "lodash";
import RestClient from '../Utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';

const { width } = Dimensions.get('window')

export default class AddNewDriver extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            age: '',
            contact: '',
            vehicle_type: '',
            vehicle_number: '',
            address: '',
            dob: '',
            tokenValue: ''
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    createDriver() {
        Keyboard.dismiss()
        let { name, age, contact, vehicle_type, vehicle_number, address, dob } = this.state;

        if (_.isEmpty(name && name.trim())) {
            alert('Please enter your name');
            return;
        }

        if (_.isEmpty(age && age.trim())) {
            alert('Please enter your age');
            return;
        }

        if (_.isEmpty(contact && contact.trim())) {
            alert('Please enter your contact');
            return;
        }

        if (_.isEmpty(vehicle_type && vehicle_type.trim())) {
            alert('Enter your Vehicle type');
            return;
        }

        if (_.isEmpty(vehicle_number && vehicle_number.trim())) {
            alert('Write your vehicle number');
            return;
        }

        if (_.isEmpty(address && address.trim())) {
            alert('Write your address');
            return;
        }

        // if (_.isEmpty(dob && dob.trim())) {
        //     alert('Write your date of birth');
        //     return;
        // }

        RestClient.post("api/vendor/delivery_boy/create", {
            name: this.state.name,
            age: this.state.age,
            contact: this.state.contact,
            vehicle_type: this.state.vehicle_type,
            vehicle_number: this.state.vehicle_number,
            address: this.state.address,
            dob: 'null',
        }, this.state.tokenValue).then((result) => {
            console.log("result", result)
            if (result.success == "1") {
                alert(result.message + "\n" + "Password: " + result.password)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <View style={{ flex: 1 }}>
                    <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>
                        <ScrollView style={{ flex: 1 }} keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>

                            <FormTextInput
                                autoFocus={false}
                                ref='name'
                                placeHolderText='Name'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(name) => this.setState({ name })}
                                textColor={Constants.Colors.White}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='age'
                                placeHolderText='Age'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(age) => this.setState({ age })}
                                textColor={Constants.Colors.White}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='contact'
                                placeHolderText='Contact'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                //secureText={true}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(contact) => this.setState({ contact })}
                                textColor={Constants.Colors.White}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='vehicle_type'
                                placeHolderText='Vehicle Type'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                //secureText={true}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(vehicle_type) => this.setState({ vehicle_type })}
                                textColor={Constants.Colors.White}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='vehicle_number'
                                placeHolderText='Vehicle Number'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(vehicle_number) => this.setState({ vehicle_number })}
                                textColor={Constants.Colors.White}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='address'
                                placeHolderText='Address'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(address) => this.setState({ address })}
                                textColor={Constants.Colors.White}
                            />

                            {/* <FormTextInput
                                autoFocus={false}
                                ref='dob'
                                placeHolderText='DOB'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(dob) => this.setState({ dob })}
                                textColor={Constants.Colors.White}
                            /> */}

                            <SubmitButton
                                style={{ marginBottom: 10 }}
                                onPress={() => this.createDriver()}
                                text="Register"
                            />
                        </ScrollView>
                    </KeyboardAvoidingView>

                </View>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})