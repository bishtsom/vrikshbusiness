"use strict";
import Connection from "../Utilities/Connection";
import querystring from "querystring";
import NetInfo from "@react-native-community/netinfo";
import axios from "axios"

let logintoken = "";

class RestClient {
    static isConnected() {
        let context = this;
        return new Promise(function (fulfill, reject) {
            NetInfo.fetch()
                .then(state => {
                    console.log("check connected", state.isConnected)
                    if (state.isConnected)
                        fulfill(state.isConnected);
                    else {
                        reject(state.isConnected);
                    }
                });

        });
    }

    static post(url, params, token = '', userId = '') {
        let context = this;
        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                console.log("url request=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                fetch(Connection.getBaseUrl() + url, {
                    method: "POST",
                    timeout: 1000 * 1 * 60,
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                        "x-vendor-token": token,
                    },
                    body: JSON.stringify(params)
                }).then((response) => {
                    return response.text()
                })
                    .then(responseText => {
                        fulfill(JSON.parse(responseText));
                    }).catch(error => {
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding'});
                        console.warn("eroro", error);
                    });
            }).catch(error => {
                console.warn("eroro catch:", error);
                fulfill({ message: 'Check your internet connectivity or our server is not responding.'});
            });
        });
    }

    static put(url, params, token = '', userId = '') {
        let context = this;

        return new Promise(function (fulfill, reject) {
            context.isConnected()
                .then(() => {
                    let query = querystring.stringify(userId);

                    // console.log("Array length ",params.length )
                    console.log("url=> ",Connection.getBaseUrl() + url ," requestObject=> ",params, " x-auth-token => ",token, " x-user-id => ",userId )
                    fetch(Connection.getBaseUrl() + url+ "?" + query, {
                        method: "PUT",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "x-vendor-token": token,
                            //"x-user-id": userId
                        },
                        body: JSON.stringify(params)
                    })
                    .then(function (response) {
                        return response.json();
                    })
                    .then(responseJson => {
                        fulfill(responseJson);
                    })
                        // .then((response) => {
                        //     //console.log('responseText*****11', response);
                        //     return response.json();
                        // })
                        // .then(responseText => {
                        //     //console.log('responseText*****', responseText);
                        //     fulfill(JSON.parse(responseText));
                        // })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    console.warn(error);
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }

    static putQuery(url, params, token = '', userId = '') {
        let context = this;
        return new Promise(function (fulfill, reject) {
            context.isConnected()
                .then(() => {
                    let query = querystring.stringify(params);
                    // console.log("Array length ",params.length )
                    console.log("url=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                    fetch(Connection.getBaseUrl() + url + "?" + query, {
                        method: "PUT",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "x-vendor-token": token,
                            //"x-user-id": userId
                        },
                        //body: JSON.stringify(params)
                    })
                        .then(function (response) {
                            return response.json();
                        })
                        .then(responseJson => {
                            fulfill(responseJson);
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    console.warn(error);
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }

    static patch(url, params, token = '', userId = '') {
        let context = this;

        return new Promise(function (fulfill, reject) {
            context.isConnected()
                .then(() => {
                    // console.log("url=> ",Connection.getBaseUrl() + url ," requestObject=> ",params, " x-auth-token => ",token, " x-user-id => ",userId )
                    fetch(Connection.getBaseUrl() + url + params, {
                        method: "PATCH",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "x-auth-token": token,
                            "x-user-id": userId
                        },
                        // body: JSON.stringify(params)
                    })
                        .then((response) => {
                            //console.log('responseText*****11', response);
                            return response.text()
                        })
                        .then(responseText => {
                            //console.log('responseText*****', responseText);
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }


    static get(url, params, token = '') {
        let context = this;

        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);
                    console.log("url get req=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " query ", query)
                    fetch(Connection.getBaseUrl() + url + "?" + query, {
                        method: "GET",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "x-vendor-token": token,
                        }
                    })
                        .then(function (response) {
                            return response.json();
                        })
                        .then(responseJson => {
                            fulfill(responseJson);
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }

    static imageUpload(url, params, token = '', userId = '') {
        console.log("image upload upload:", params)
        let context = this,
            logintoken;


        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                // console.log("url=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                fetch(Connection.getBaseUrl() + url, {
                    method: "POST",
                    headers: {
                        // 'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                        "x-vendor-token": token,
                    },
                    body: params
                })
                    .then((response) => {
                        return response.text()
                    })
                    .then(responseText => {
                        console.log('response ******** ', responseText)
                        fulfill(JSON.parse(responseText));
                    })
                    .catch(error => {
                        console.warn(error);
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                    });
            })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }



    static imageUpload1(url, params, token = '', userId = '') {
        let context = this,
            logintoken;


        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                // console.log("url=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                fetch(Connection.getBaseUrl() + url, {
                    method: "POST",
                    headers: {
                        // 'Accept': 'application/json',
                        //  'Content-Type': 'multipart/form-data',
                        "token": token,
                        // "x-user-id": userId
                    },
                    body: params
                })
                    .then((response) => {
                        return response.text()
                    })
                    .then(responseText => {
                        console.log('response ******** ', responseText)
                        fulfill(JSON.parse(responseText));
                    })
                    .catch(error => {
                        console.warn(error);
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                    });
            })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }



    static putimageUpload(url, params, token = '', userId = '') {
        let context = this,
            logintoken;


        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                // console.log("url=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                fetch(Connection.getBaseUrl() + url, {
                    method: "PUT",
                    headers: {
                        // 'Accept': 'application/json',
                        //'Content-Type': 'multipart/form-data',
                        "token": token,
                        //  "x-user-id": userId
                    },
                    body: params
                })
                    .then((response) => {
                        return response.text()
                    })
                    .then(responseText => {
                        console.log('response ******** ', responseText)
                        fulfill(JSON.parse(responseText));
                    })
                    .catch(error => {
                        console.warn(error);
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                    });
            })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }
    static sample(url, params, token = '', userId = '') {
        let context = this,
            logintoken;


        const headerObj = {
            headers: {
                'Content-Type': 'multipart/form-data',
                "token": token
            }
        };
        //    let headers = {
        //         'Content-Type': 'multipart/form-data',
        //         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJuaXNoYXMiLCJpZCI6IjViMDdlOTg4NDgwODQ4NGExZGFkYTZkNyIsImlhdCI6MTUyOTQwMzQ5N30.Eeo9sQ_sel7vXWjhJDPpa46XEviU2JrLhTgzucuOd7k"
        //     };
        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                // console.log("url=> ", Connection.getBaseUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                axios.post(
                    Connection.getBaseUrl() + url, params, headerObj
                )
                    .then((response) => {
                        console.log(response, 'RESPONSE *******')
                        return response;
                    })
                    // .then(responseText => {
                    //     console.log('response ******** ',responseText)
                    //     fulfill(JSON.parse(responseText));
                    // })
                    .catch(error => {
                        console.log(error, 'catch 1')
                        fulfill({ message: error });
                    });
            })
                .catch(error => {
                    console.log(error, 'catch 2')
                    fulfill({ message: error });
                });
        });
    }


    static delete(url, params, token = '', userId = '') {
        let context = this,
            logintoken;

        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                //console.log("url=> ",Connection.getBaseUrl() + url ," requestObject=> ",params, " x-auth-token => ",token, " x-user-id => ",userId )
                fetch(Connection.getBaseUrl() + url, {
                    method: "DELETE",
                    timeout: 1000 * 1 * 60,
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "x-auth-token": token,
                        "x-user-id": userId
                    },
                    body: JSON.stringify(params)
                })
                    .then((response) => {
                        return response.text()
                    })
                    .then(responseText => {
                        //console.log('responseText*****',responseText);
                        fulfill(JSON.parse(responseText));
                    }).catch(error => {
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        console.warn(error);
                    });
            }).catch(error => {
                fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
            });
        });
    }

    static get_New(url, params, type) {
        var connectionString = Connection.getBaseUrl();
        if (type == 'admin')
            connectionString = Connection.getBaseUrl();
        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);

                    console.log(connectionString + url + '?' + query);
                    fetch(connectionString + url + '?' + query, {
                        method: "GET",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                        }
                    })
                        .then((response) => {

                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.'});
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding'});
                });
        });
    }

    static getUrlAdmin(url, params) {
        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);
                    fetch(Connection.getBaseUrl() + url + "?" + query, {
                        method: "GET",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            // "token": token,
                            // "x-user-id": userId
                        }
                    })
                        .then((response) => {
                            console.log('!!!!!!!!!!!!!!!!!!!.......restclient--response.text()')
                            return response.text()
                        })
                        .then(responseText => {
                            //console.log('responseText*****',responseText);
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.'});
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.'});
                });
        });
    }


    static get_New_Post(url, params, token) {
        var connectionString = Connection.getBaseUrl();
        
        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);
                    console.log(connectionString + url + '?' + query);
                    fetch(connectionString + url + '?' + query, {
                        method: "POST",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "x-vendor-token": token,
                        }
                    })
                        .then((response) => {
                            return response.json()
                        })
                        .then(responseText => {
                            fulfill(responseText);
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }


    static get_New_Patch(url, params, token = '') {
        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    //let query = querystring.stringify(params);
                    //console.log("get_New_Patch request:", JSON.stringify(params));

                    fetch(Connection.getBaseUrl() + url, {
                        method: "Patch",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "token": token,
                        },
                        body: JSON.stringify(params)
                    })
                        .then((response) => {
                            //console.log("get_New_Patch response", JSON.stringify(response))
                            return response.text()
                        })
                        .then(responseText => {
                            //console.log('get_New_Patch responseText*****', responseText);

                            //      alert(JSON.stringify(responseText))
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }


    static post_Synch(url, params, token = '', userId = '') {
        let context = this,
            logintoken;




        //alert('http://13.127.98.44:8283/api/'+url);

        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                fetch(Connection.getBaseUrl() + '/' + url, {
                    method: "POST",
                    timeout: 1000 * 1 * 60,
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(params)
                }).then((response) => {
                    console.log(response);
                    return response.text()
                })
                    .then(responseText => {

                        fulfill(JSON.parse(responseText));
                    }).catch(error => {
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                    });
            }).catch(error => {
                fulfill({ message: 'Please check your internet connectivity or our server is not responding.'});
            });
        });
    }



    static getFromGoogle(orgin, destiantion) {


        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                fetch('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + orgin + '&destinations=' + destiantion + '&key=AIzaSyAQFeoAvJ9Dgz2O-E9E1hx9Tuf6ofPy9Lg', {
                    method: "GET",
                    timeout: 1000 * 1 * 60,
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(params)
                }).then((response) => {
                    console.log(response);
                    return response.text()
                })
                    .then(responseText => {

                        fulfill(JSON.parse(responseText));
                    }).catch(error => {
                        console.log(error);
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.'  });
                    });
            }).catch(error => {
                fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
            });
        });
    }

    static getNewRoute(url, params) {
        var connectionString = Connection.getBaseUrl();

        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);

                    console.log("order by schedule: ", connectionString + url + '?' + query);
                    fetch(connectionString + url + '?' + query, {
                        method: "GET",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                        }
                    })
                        .then((response) => {

                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.'  });
                });
        });
    }

    static getNewRouteCustomer(url, params) {
        var connectionString = Connection.getBaseUrl();

        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);

                    console.log("order by schedule: ", connectionString + url + '?' + query);
                    fetch(connectionString + url + '?' + query, {
                        method: "GET",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                        }
                    })
                        .then((response) => {

                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }

    static postRoutePlanner(url, params, token = '', userId = '') {
        let context = this,
            logintoken;
        // console.log("restClientUrl", url)
        // console.log("restClientUrl params", params)
        // console.log("restClientUrl token", token)
        // console.log("restClientUrl userId", userId)

        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {
                // console.log("url=> ", Connection.getRoutePlannerRestUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                fetch(Connection.getBaseUrl() + url, {
                    method: "POST",
                    timeout: 1000 * 1 * 60,
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "token": token,
                        "x-user-id": userId
                    },
                    body: JSON.stringify(params)
                }).then((response) => {
                    return response.text()
                })
                    .then(responseText => {
                        //console.log('responseText*****', responseText, "********* ", url);
                        fulfill(JSON.parse(responseText));
                    }).catch(error => {
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        console.warn("eroro", error);
                    });
            }).catch(error => {
                fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
            });
        });
    }

    static postRoutePlannerFormData(url, params, token = '', userId = '') {
        let context = this,
            logintoken;
        // console.log("restClientUrl", url)
        // console.log("restClientUrl params", params)
        // console.log("restClientUrl token", token)
        // console.log("restClientUrl userId", userId)

        return new Promise(function (fulfill, reject) {
            context.isConnected().then(() => {

                // console.log("url=> ", Connection.getRoutePlannerRestUrl() + url, " requestObject=> ", params, " x-auth-token => ", token, " x-user-id => ", userId)
                fetch(Connection.getBaseUrl() + url, {
                    method: "POST",
                    timeout: 1000 * 1 * 60,
                    // headers: {
                    //     Accept: "application/json",
                    //     "Content-Type": "application/json",
                    //     "token": token,
                    //     "x-user-id": userId
                    // },
                    body: params
                }).then((response) => {
                    return response.text()
                })
                    .then(responseText => {
                        //console.log('responseText*****', responseText, "********* ", url);
                        fulfill(JSON.parse(responseText));
                    }).catch(error => {
                        fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        console.warn("eroro", error);
                    });
            }).catch(error => {
                console.log("response error", error)
                fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
            });
        });
    }

    static patchRoutePlaner(url, params, token = '') {
        let context = this;
        return new Promise(function (fulfill, reject) {
            context
                .isConnected()
                .then(() => {
                    let query = querystring.stringify(params);
                    console.log(JSON.stringify(params));

                    // fetch(Connection.getRoutePlannerRestUrl() + url, {
                    fetch(Connection.getBaseUrl() + url, {
                        method: "Patch",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "token": token,
                        },
                        body: JSON.stringify(params)
                    })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            console.log('responseText*****', responseText);
                            //      alert(JSON.stringify(responseText))
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.'  });
                });
        });
    }


    static patchCustomerApiSignature(url, params, deviceToken) {
        let context = this;
        console.log("url", url)
        console.log("params:", params)
        console.log("patchCustomerApiSignature device token:", deviceToken)

        return new Promise(function (fulfill, reject) {
            context.isConnected()
                .then(() => {
                    fetch(Connection.getBaseUrl() + url, {
                        method: "Patch",
                        timeout: 1000 * 1 * 60,
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "token": deviceToken,
                        },
                        body: JSON.stringify(params)
                    })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn("error now", error);
                            console.warn(error);
                            fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                        });
                })
                .catch(error => {
                    console.warn("error now1", error);
                    fulfill({ message: 'Please check your internet connectivity or our server is not responding.' });
                });
        });
    }
}

export default RestClient;
