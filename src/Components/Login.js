import React, { Component } from 'react'
import { Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../Utilities/Regex';
import _ from "lodash";
import RestClient from '../Utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';
const { width } = Dimensions.get('window')
import { pushNotificationInit, pushNotificationRemove } from "../Utilities/PushNotification";
import { NavigationActions, StackActions } from "react-navigation"

const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' })
    ], key: null
});

export default class Login extends Component {
    constructor(props) {
        super()
        this.state = {
            email: '',
            password: '',
            //email: 'vendor@abc.com',
            //password: '0000',
        }
    }

    loginUser() {
        Keyboard.dismiss()
        let { email, password } = this.state;

        if (_.isEmpty(email && email.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(email && email.trim())) {
            alert('Enter a valid email');
            return;
        }

        if (_.isEmpty(password)) {
            alert('Please enter your password');
            return;
        }

        // AsyncStorage.getItem("pushToken").then((pushToken) => {
        console.log("push token are", Constants.registrationId)
        RestClient.post("login/vendor", { email: this.state.email, password: this.state.password, registration_id: Constants.registrationId }).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                AsyncStorage.setItem("token", result.token)
                console.log("token", JSON.stringify(result.token))
                this.props.navigation.dispatch(resetActionLogin)
                //this.props.navigation.navigate("Home")
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
        // })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        <View style={{height: 30}} />

                        <Image source={Constants.Images.logo} style={styles.logo} resizeMode={'contain'} />
                        <FormTextInput
                            autoFocus={false}
                            ref='email'
                            placeHolderText='Email'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            secureText={false}
                            keyboard='email-address'
                            returnKey='done'
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(email) => this.setState({ email })}
                            textColor={Constants.Colors.White}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='password'
                            placeHolderText='Password'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            secureText={true}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(password) => this.setState({ password })}
                            textColor={Constants.Colors.White}
                        />

                        <SubmitButton
                            onPress={() => this.loginUser()}
                            //onPress={() => this.props.navigation.navigate("Home")}
                            text="Sign In"
                        />
                    </KeyboardAvoidingView>

                    <TouchableOpacity onPress={() => navigate("SignIn")}>
                        <Text style={[styles.register, { color: Constants.Colors.Orange }]}>
                            {`Want to become a vendor?`}
                        </Text>
                    </TouchableOpacity>

                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 50,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 50,
        alignSelf: 'center',
        marginTop: 40,
        marginBottom: 20
    },
    register: {
        fontSize: 16,
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
})