import React, { Component } from 'react'
import { Keyboard, Dimensions, Modal, Image, StyleSheet, Text, View, ScrollView, TouchableHighlight, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../Utilities/Regex';
import _ from "lodash";
import RestClient from '../Utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';
import Connection from "../Utilities/Connection";
import ImagePickerCropper from "react-native-image-crop-picker";
import { Dropdown } from 'react-native-material-dropdown';

const { width } = Dimensions.get('window')

export default class MyProfile extends Component {
    constructor(props) {
        super()
        this.state = {
            city: '',
            locality: '',
            state: '',
            name: '',
            houseNo: '',
            landmark: '',
            pincode: '',
            email: '',
            contact: '',
            store_name: '',
            prof_img: '',
            vendor_id: '',
            tokenValue: '',
            imageType: '',
            ImagemodalVisible: false,
            profilePic: {},
            cities: [],
            selectedCity: ''

        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getCities();
    }

    getCities() {
        let city = []
        RestClient.get("city", {}).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.cities.length; i++) {
                    city.push({ value: result.cities[i].charAt(0).toUpperCase() + result.cities[i].slice(1) })
                }
            }
            this.setState({ cities: city })
            this.getNewDetails();
        }).catch(error => {
            this.getNewDetails();
        });
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/profile", {}, tokenValue).then((result) => {
                console.log("customer details:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({
                        name: result.vendor.name,
                        email: result.vendor.email,
                        contact: result.vendor.contact,
                        city: result.vendor.city,
                        selectedCity: result.vendor.city,
                        houseNo: result.vendor.houseNo,
                        landmark: result.vendor.landmark,
                        locality: result.vendor.locality,
                        pincode: result.vendor.pincode,
                        state: result.vendor.state,
                        store_name: result.vendor.store_name
                    })
                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    saveProfile() {
        Keyboard.dismiss()
        let { name, email, contact, selectedCity, store_name, city, houseNo, locality, landmark, pincode, state, vendor_id } = this.state;
        let req = null
        if (_.isEmpty(name && name.trim())) {
            alert('Please enter your name');
            return;
        }

        if (_.isEmpty(email && email.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(email && email.trim())) {
            alert('Enter a valid email');
            return;
        }

        if (_.isEmpty(contact)) {
            alert('Please enter your contact');
            return;
        }

        if (_.isEmpty(houseNo && houseNo.trim())) {
            alert('Please enter your house number');
            return;
        }

        if (_.isEmpty(landmark && landmark.trim())) {
            alert('Please specfiy your address landmark');
            return;
        }

        if (_.isEmpty(locality && locality.trim())) {
            alert('Please specfiy your address locality');
            return;
        }

        if (_.isEmpty(selectedCity && selectedCity.trim())) {
            alert('Please specfiy your city');
            return;
        }

        if (_.isEmpty(state && state.trim())) {
            alert('Please specfiy your state');
            return;
        }

        if (_.isEmpty(pincode && pincode.trim())) {
            alert('Write your PinCode');
            return;
        }

        if (_.isEmpty(store_name && store_name.trim())) {
            alert('Write your store name');
            return;
        }

        req = {
            houseNo: houseNo,
            landmark: landmark,
            locality: locality,
            city: selectedCity.toLowerCase(),
            state: state,
            pincode: pincode,
            name: name,
            email: email,
            contact: contact,
            store_name: store_name
        }

        RestClient.put("api/vendor/update",
            req
            , this.state.tokenValue, vendor_id).then((result) => {
                console.log("result", result)
                if (result.success == "1") {
                    alert(result.message)
                    if (key == '1') {
                        AsyncStorage.setItem("name", name)
                    }
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
    }

    openImagePickerCropper = (imageType) => {
        ImagePickerCropper.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            this.setState({ ImagemodalVisible: false })
            let source = { uri: image.path };
            let fileName = image.path.split("/")
            let len = fileName.length;
            let name = fileName[len - 1]
            if (this.state.imageType == 'profile') {
                this.setState({
                    ProfileImageSource: source,
                    profilePic: {
                        uri: image.path,
                        name: name,
                        filename: name,
                        type: image.mime
                    }
                })
                this.saveProfileImage();
            }
        })
    }

    openImagePickerCropperCamera = (imageType) => {
        ImagePickerCropper.openCamera({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            this.setState({ ImagemodalVisible: false })
            let source = { uri: image.path };
            let fileName = image.path.split("/")
            let len = fileName.length;
            let name = fileName[len - 1]
            if (this.state.imageType == 'profile') {

                this.setState({
                    ProfileImageSource: source,
                    profilePic: {
                        uri: image.path,
                        name: name,
                        filename: name,
                        type: image.mime
                    }
                })
                this.saveProfileImage();
            }
        })
    }

    saveProfileImage() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            const body = new FormData();
            body.append("file", { uri: this.state.profilePic.uri })
            RestClient.imageUpload("api/vendor/prof_pic/upload", body, tokenValue).then((result) => {
                this.setState({ prof_img: this.state.profilePic.uri })
                console.log("image", result)
            }).catch((error) => {
                console.log("error in drawer", error)
            })
        })
    }


    render() {
        const { navigate } = this.props.navigation
        return (
            <SafeAreaView style={styles.containerSafe}>
                <Background style={styles.container}>
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView style={{ flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>

                            {/* <TouchableOpacity style={[styles.navIcons, { marginLeft: 20, marginTop: 20, }]} onPress={() => this.setState({ ImagemodalVisible: true, imageType: 'profile' })}>
                                <Image source={{ uri: `${Connection.getBaseUrl()}/${this.state.prof_img}` }}
                                    style={styles.navIcons} resizeMode={'contain'} />
                            </TouchableOpacity> */}

                            <FormTextInput
                                autoFocus={false}
                                ref='name'
                                placeHolderText='Name'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(name) => this.setState({ name })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                value={this.state.name}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='email'
                                placeHolderText='Email'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(email) => this.setState({ email })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                value={this.state.email}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='contact'
                                placeHolderText='Contact'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                //secureText={true}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(contact) => this.setState({ contact })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                value={this.state.contact}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='houseNo'
                                placeHolderText='House No'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(houseNo) => this.setState({ houseNo })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                value={this.state.houseNo}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='landmark'
                                placeHolderText='Landmark'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(landmark) => this.setState({ landmark })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.landmark}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='locality'
                                placeHolderText='Locality'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(locality) => this.setState({ locality })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.locality}
                            />

                            {/* <FormTextInput
                                autoFocus={false}
                                ref='city'
                                placeHolderText='City'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(city) => this.setState({ city })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.city}
                            /> */}

                            <Dropdown
                                containerStyle={[{ width: width - 45, alignSelf: 'center' }]}
                                baseColor={Constants.Colors.White}
                                textColor={Constants.Colors.White}
                                selectedItemColor={Constants.Colors.DarkBlue}
                                label='Select City'
                                fontSize={16}
                                data={this.state.cities}
                                value={this.state.selectedCity}
                                onChangeText={(text) => { this.setState({ selectedCity: text }) }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='state'
                                placeHolderText='State'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(state) => this.setState({ state })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.state}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='pincode'
                                placeHolderText='PinCode'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(pincode) => this.setState({ pincode })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.pincode}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='outlet_name'
                                placeHolderText='Outlet/Store Name'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(store_name) => this.setState({ store_name })}
                                textColor={Constants.Colors.White}
                                style={{ marginVertical: 5 }}
                                value={this.state.store_name}
                            />

                            <SubmitButton
                                onPress={() => this.saveProfile()}
                                text="Update"
                            //style={{ alignSelf: 'flex-end', marginTop: 0 }}
                            //backGroundStyle={{ width: width / 4, padding: 8 }}
                            />

                            <Modal animationType={"slide"} transparent={true}
                                visible={this.state.ImagemodalVisible}
                                onRequestClose={() => { console.log("Modal has been closed.") }}>
                                <View style={{ flex: 8 }}></View>
                                <View style={{ flex: 2, backgroundColor: "white" }}>
                                    <TouchableHighlight
                                        style={{ flex: 1, borderBottomWidth: 1, borderColor: "gray", justifyContent: "center" }}
                                        onPress={() => {
                                            this.openImagePickerCropper(this.state.imageType)
                                        }}>
                                        <Text style={styles.text}>Select From Gallery..</Text>
                                    </TouchableHighlight>

                                    <TouchableHighlight
                                        style={{ flex: 1, borderBottomWidth: 1, borderColor: "gray", justifyContent: "center" }}
                                        onPress={() => {
                                            this.openImagePickerCropperCamera(this.state.imageType)
                                        }}>
                                        <Text style={styles.text}>Open Camera..</Text>
                                    </TouchableHighlight>

                                    <TouchableHighlight
                                        style={{ flex: 1, borderBottomWidth: 1, borderColor: "gray", justifyContent: "center" }}
                                        onPress={() => { this.setState({ ImagemodalVisible: !this.state.ImagemodalVisible }) }}>
                                        <Text style={styles.text}>Cancel</Text>
                                    </TouchableHighlight>
                                </View>
                            </Modal>

                        </KeyboardAvoidingView>
                    </ScrollView>
                    {/* </View> */}
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
    navIcons: {
        height: 100,
        width: 100,
        //borderColor: Constants.Colors.White,
        //borderWidth: 2,
        //backgroundColor: Constants.Colors.White,
        borderRadius: 100 / 2
    },
    text: {
        fontSize: 18,
        alignSelf: "center"
    },
})