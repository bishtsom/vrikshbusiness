import React, { Component } from 'react'
import { FlatList, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';

const { width } = Dimensions.get('window')
const { months } = ["January", "February", "March", "April",
    "May", "June", "July", "August", "September", "October",
    "November", "December"];
const { weekDays } = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const { nDays } = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

export default class MonthTableChart extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            currentMonthSold: '',
            previousOrders: []
        }
        this.months = ["January", "February", "March", "April",
            "May", "June", "July", "August", "September", "October",
            "November", "December"];
        this.weekDays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        this.nDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        const { navigation } = this.props;
        const year = navigation.getParam('year', new Date().getFullYear());
        const month = navigation.getParam('month');

        var maxDays = this.nDays[month - 1];
        if (month == 2) { // February
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                maxDays += 1;
            }
        }
        let previousOrdersList = []
        AsyncStorage.getItem("token").then((tokenValue) => {
            RestClient.get("api/vendor/order/previous/month", { year: year, month: month }, tokenValue).then((result) => {
                console.log("vendor new:", JSON.stringify(result));
                if (result.success == "1") {
                    for (let i = 1; i <= maxDays; i++) {
                        previousOrdersList.push({
                            date: i,
                            result: 0
                        })
                    }
                    console.log("previous list length:", JSON.stringify(result.Previous_Orders.length));

                    for (let i = 0; i < previousOrdersList.length; i++) {
                        for (let j = 0; j < result.Previous_Orders.length; j++) {
                            for (let k = 0; k < result.Previous_Orders[j].length; k++) {
                                //console.log("previous list date:", result.Previous_Orders[j][k].complete_date);
                                if (new Date(result.Previous_Orders[j][k].complete_date).getDate() == previousOrdersList[i].date) {
                                    previousOrdersList[i].date = i;
                                    previousOrdersList[i].result = result.Previous_Orders[j];
                                }
                            }
                        }
                    }
                    this.setState({ previousOrders: previousOrdersList })
                    console.log("prev res", this.state.previousOrders)
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    renderBottomButtons(item) {
        let totalBill = 0
        if(item.result != 0){
            for(let i =0; i< item.result.length; i++){
                totalBill = totalBill + Number(item.result[i].total_bill);
            }
        }
        return (
            <Text style={{ color: 'white' }}>{item.result == 0 ? null : totalBill}</Text>
        )
    }

    render() {
        const { navigate } = this.props.navigation
        const { navigation } = this.props;
        const year = navigation.getParam('year', new Date().getFullYear());
        const month = navigation.getParam('month');
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 5 }}>

                        <View style={styles.logo} />

                        <FlatList
                            data={this.state.previousOrders}
                            renderItem={({ item }) => (
                                <TouchableOpacity onPress={() => item.result == 0 ? null : navigate('MonthTableChartDetail', { dayData: item.result })} style={[styles.productBox, { flexDirection: 'column', justifyContent: 'center', flex: 1 }]}>
                                    <Text style={{ color: 'white' }}>{item.date}</Text>
                                    {this.renderBottomButtons(item)}

                                </TouchableOpacity>
                            )}
                            numColumns={7}
                            keyExtractor={(item, index) => index.toString()}
                        />

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        width: 60,
        borderRadius: 5,
        padding: 2,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})