import React, { Component } from 'react'
import { Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../Utilities/Regex';
import _ from "lodash";
import RestClient from '../Utilities/RestClient';
import { NavigationActions, StackActions } from "react-navigation"
import { Dropdown } from 'react-native-material-dropdown';

const { width } = Dimensions.get('window')
const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Login' })
    ], key: null
});
export default class SignIn extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            email: '',
            contact: '',
            outletName: '',
            outletAddress: '',
            password: '',
            houseNo: '',
            landmark: '',
            locality: '',
            city: '',
            state: '',
            pincode: '',
            cities: [],
        }
    }

    componentDidMount() {
        this.getCities();
    }

    getCities() {
        let city = []
        RestClient.get("city", {}).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.cities.length; i++) {
                    city.push({ value: result.cities[i].charAt(0).toUpperCase() + result.cities[i].slice(1) })
                }
            }
            this.setState({ cities: city })
        }).catch(error => {
        });
    }

    registerUser() {
        Keyboard.dismiss()
        let { name, email, password, contact, outletName, outletAddress, houseNo, landmark, locality, city, state, pincode } = this.state;
        console.log('city name', city)

        if (_.isEmpty(name && name.trim())) {
            alert('Please enter your name');
            return;
        }

        if (_.isEmpty(email && email.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(email && email.trim())) {
            alert('Enter a valid email');
            return;
        }

        if (_.isEmpty(password)) {
            alert('Please enter your password');
            return;
        }

        if (_.isEmpty(contact)) {
            alert('Enter your contact number');
            return;
        }

        if (_.isEmpty(outletName && outletName.trim())) {
            alert('Write your outlet name');
            return;
        }

        // if (_.isEmpty(outletAddress && outletAddress.trim())) {
        //     alert('Write your outlet address');
        //     return;
        // }

        if (_.isEmpty(houseNo && houseNo.trim())) {
            alert('Please enter your house number');
            return;
        }

        if (_.isEmpty(landmark && landmark.trim())) {
            alert('Please write your landmark location');
            return;
        }

        if (_.isEmpty(locality && locality.trim())) {
            alert('Please write your locality');
            return;
        }

        if (_.isEmpty(city && city.trim())) {
            alert('Enter your City name');
            return;
        }

        if (_.isEmpty(state && state.trim())) {
            alert('Please specfiy your address state');
            return;
        }

        if (_.isEmpty(pincode && pincode.trim())) {
            alert('Please write your pincode');
            return;
        }

        RestClient.post("api/vendor/create", {
            name: name, email: email, password: password, contact: contact, store_name: outletName, houseNo: houseNo,
            landmark: landmark,
            locality: locality,
            city: city.toLowerCase(),
            state: state,
            pincode: pincode,
        }).then((result) => {
            console.log("result", result)
            if (result.success == "1") {
                this.props.navigation.dispatch(resetActionLogin)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <View >
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView style={{ flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>

                            <FormTextInput
                                autoFocus={false}
                                ref='name'
                                placeHolderText='Name'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(name) => this.setState({ name })}
                                textColor={Constants.Colors.White}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='email'
                                placeHolderText='Email'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(email) => this.setState({ email })}
                                textColor={Constants.Colors.White}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='password'
                                placeHolderText='Password'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                //secureText={true}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(password) => this.setState({ password })}
                                textColor={Constants.Colors.White}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='contact'
                                placeHolderText='Contact Number'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                //secureText={true}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(contact) => this.setState({ contact })}
                                textColor={Constants.Colors.White}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='outlet_name'
                                placeHolderText='Outlet/Store Name'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(outletName) => this.setState({ outletName })}
                                textColor={Constants.Colors.White}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='houseNo'
                                placeHolderText='House No'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(houseNo) => this.setState({ houseNo })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='landmark'
                                placeHolderText='Landmark'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(landmark) => this.setState({ landmark })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='locality'
                                placeHolderText='Locality'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(locality) => this.setState({ locality })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            {/* <FormTextInput
                                autoFocus={false}
                                ref='city'
                                placeHolderText='City'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(city) => this.setState({ city })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            /> */}

                            <Dropdown
                                containerStyle={[{ width: width - 45, alignSelf: 'center' }]}
                                baseColor={Constants.Colors.White}
                                textColor={Constants.Colors.White}
                                selectedItemColor={Constants.Colors.DarkBlue}
                                label='Select City'
                                fontSize={16}
                                data={this.state.cities}
                                //value={this.state.selectedCity}
                                onChangeText={(text) => { this.setState({ city: text }) }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='state'
                                placeHolderText='State'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(state) => this.setState({ state })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='pincode'
                                placeHolderText='PinCode'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(pincode) => this.setState({ pincode })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <SubmitButton
                                onPress={() => this.registerUser()}
                                text="Register"
                            />

                            <TouchableOpacity onPress={() => navigate("Login")}>
                                <Text style={[styles.register, { color: Constants.Colors.Orange }]}>
                                    {`Already a vendor?`}
                                </Text>
                            </TouchableOpacity>
                        </KeyboardAvoidingView>
                    </ScrollView>

                </View>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: 15
    },
})