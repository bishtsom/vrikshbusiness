import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';

const { width } = Dimensions.get('window')

export default class Complaints extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            complainsList: [],
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getComplainsList();
    }

    getComplainsList() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/complains", {}, tokenValue).then((result) => {
                console.log("complains result:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ complainsList: result.Complains })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{margin: 20}}>

                        {this.state.complainsList && this.state.complainsList.length > 0 && this.state.complainsList.map((data, key) => {
                            return (
                                <TouchableOpacity onPress={() => navigate('ComplaintDetail', {complaintDetail : data })} key={key} style={[styles.productBox,{margin: 5, flexDirection: 'row', justifyContent: 'space-between', flex: 1 }]}>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{data.title}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{data.complain_id}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})