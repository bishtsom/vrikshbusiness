'use strict';
const
    //live = "organicmulti.pythonanywhere.com",     // original
    live = "thevriksh.in",
    apiBase_url = `https://${live}/`;

    // live = "driverapi.delgate.com",     // original
    // running_url = live,
    // apiBase_url = `http://${running_url}/api/`;


export default class Connection {
    static getBaseUrl() {
        return apiBase_url;
    };
}
