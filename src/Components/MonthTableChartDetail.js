import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import Background from '../common/Background';

const { width } = Dimensions.get('window')

export default class MonthlyTableChartDetail extends Component {
    constructor(props) {
        super()
        this.state = {
            previousOrderDetailList: [],
        }
    }

    componentWillMount() {
    }

    render() {
        const { navigation } = this.props;
        const dayData = navigation.getParam('dayData');
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 10 }}>

                        {dayData && dayData.length > 0 && dayData.map((data, key) => {
                            return (
                                <View key={key} style={[styles.productBox, { margin: 5, flexDirection: 'column', flex: 1 }]}>
                                    {/* <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Product id : {data.product_id}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Quantity : {data.quantity}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Total Price. : {data.total_price}</Text> */}
                                    
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Name : {data.customer_name}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Order Id : {data.order_id}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Serial No. : {data.order_id}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Total bill : {data.total_bill}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Address : {data.delivery_address}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>Product Name : {data.customer_name}</Text> 
                                </View>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        //alignItems: "center",
        //height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})