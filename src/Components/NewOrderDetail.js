import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';
import Dialog, { DialogContent, SlideAnimation, DialogTitle } from 'react-native-popup-dialog';

const { width } = Dimensions.get('window')

export default class NewOrderDetail extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: '',
            quantityList: [],
            deliveryCharges: '',
            dialogShow: false,
            deliveryBoyList: [],
            selectedDriverId: '',
            updateAll: false,
            vendorId: ''
        }
    }

    getNewDetails() {
        RestClient.get("api/vendor/profile", {}, this.state.tokenValue).then((result) => {
            console.log("customer details:", JSON.stringify(result));
            if (result.success == "1") {
                this.setState({ vendorId: result.vendor.vendor_id })
            } else {
                //alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    componentWillMount() {
        let productsQuant = []
        for (let i = 0; i < this.props.navigation.getParam('newOrders').products.length; i++) {
            productsQuant.push(this.props.navigation.getParam('newOrders').products[i].quantity)
        }
        this.setState({ newOrders: this.props.navigation.getParam('newOrders'), quantityList: productsQuant, deliveryCharges: (this.props.navigation.getParam('newOrders').total_bill - this.props.navigation.getParam('newOrders').bill) })

        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
            this.getNewDetails();
        })
    }

    selectDriver(value) {
        console.log("vendor Id", this.state.vendorId)
        RestClient.get("api/vendor/delivery_boy/all", { vendor_id: this.state.vendorId }, this.state.tokenValue).then((result) => {
            console.log("all bous new:", JSON.stringify(result));
            if (result.success == "1") {
                this.setState({ updateProducts: value, dialogShow: true, deliveryBoyList: result.drivers })
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    driverSelected(item) {
        console.log("driver id", item.driver_id)
        this.setState({ dialogShow: false, selectedDriverId: item.driver_id })
        if (this.state.updateAll) {
            this.updateProducts(item.driver_id)
        } else {
            this.saveAction('1', item.driver_id)
        }
    }

    saveAction = (buttonType, driverId) => {
        //const { navigation } = this.props;  
        //const newOrders = navigation.getParam('newOrders'); 
        RestClient.get_New_Post("api/vendor/order/new/update", { order_id: this.state.newOrders.order_id, status: buttonType, driver_id: driverId }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    updateProducts(driverId) {
        let list = [];
        list.push({
            bill: this.state.newOrders.bill + "",
            //customer_name: this.state.newOrders.customer_name,
            //delivery_address: this.state.newOrders.delivery_address,
            //order_date: this.state.newOrders.order_date,
            order_id: this.state.newOrders.order_id,
            status: "1",
            total_bill: this.state.newOrders.total_bill + "",
            driver_id: driverId
        })
        for (let i = 0; i < this.state.newOrders.products.length; i++) {
            list.push({
                product_id: this.state.newOrders.products[i].product_id,
                //quantity: 1
                quantity: this.state.newOrders.products[i].quantity + "",
                total_price: this.state.newOrders.products[i].total_price + ""
            })
        }
        console.log("put data", JSON.stringify(list))
        RestClient.put("api/vendor/order/new/update/new", list, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    sub(data, key) {
        let list = []
        for (let i = 0; i < this.state.newOrders.products.length; i++) {
            if (i == key) {
                list.push({
                    product_name: this.state.newOrders.products[i].product_name,
                    quantity: this.state.newOrders.products[i].quantity - 1,
                    total_price: this.state.newOrders.products[i].total_price - (this.state.newOrders.products[i].total_price / this.state.newOrders.products[i].quantity)
                })
            } else {
                list.push({
                    product_name: this.state.newOrders.products[i].product_name,
                    quantity: this.state.newOrders.products[i].quantity,
                    total_price: this.state.newOrders.products[i].total_price
                })
            }
        }

        let billAmount = 0;
        for (let i = 0; i < list.length; i++) {
            billAmount = Number(billAmount) + Number(list[i].total_price);
        }

        let newOrderObj = {
            products: list,
            bill: billAmount,
            customer_name: this.state.newOrders.customer_name,
            delivery_address: this.state.newOrders.delivery_address,
            order_date: this.state.newOrders.order_date,
            order_id: this.state.newOrders.order_id,
            status: this.state.newOrders.status,
            total_bill: (billAmount + this.state.deliveryCharges)
        }
        console.log("scheckkkkk", JSON.stringify(newOrderObj))

        this.setState({ newOrders: newOrderObj })
        //console.log("quantity", data.quantity - 1)
    }

    add(data, key) {
        let list = []
        for (let i = 0; i < this.state.newOrders.products.length; i++) {
            if (i == key) {
                list.push({
                    product_name: this.state.newOrders.products[i].product_name,
                    quantity: this.state.newOrders.products[i].quantity + 1,
                    total_price: this.state.newOrders.products[i].total_price + (this.state.newOrders.products[i].total_price / this.state.newOrders.products[i].quantity)
                })
            } else {
                list.push({
                    product_name: this.state.newOrders.products[i].product_name,
                    quantity: this.state.newOrders.products[i].quantity,
                    total_price: this.state.newOrders.products[i].total_price
                })
            }
        }

        let billAmount = 0;
        for (let i = 0; i < list.length; i++) {
            billAmount = Number(billAmount) + Number(list[i].total_price);
        }

        let newOrderObj = {
            products: list,
            bill: billAmount,
            customer_name: this.state.newOrders.customer_name,
            delivery_address: this.state.newOrders.delivery_address,
            order_date: this.state.newOrders.order_date,
            order_id: this.state.newOrders.order_id,
            status: this.state.newOrders.status,
            total_bill: (billAmount + this.state.deliveryCharges)
        }
        console.log("scheckkkkk", JSON.stringify(newOrderObj))

        this.setState({ newOrders: newOrderObj })
    }

    renderBottomButtons() {
        let buttonName = '10'
        for (let i = 0; i < this.state.newOrders.products.length; i++) {
            if (this.state.newOrders.products[i].quantity != this.state.quantityList[i]) {
                buttonName = '20'
                break
            }
        }
        return (
            <TouchableOpacity onPress={() => buttonName == 10 ? this.selectDriver(false) : this.selectDriver(true)} style={[styles.goToSignInOuter, { backgroundColor: Constants.Colors.Orange }]}>
                <Text style={[styles.goToSignInInner]}>{buttonName == 10 ? 'Approve All' : 'Update All'}</Text>
            </TouchableOpacity>
        )
    }

    closePopUp() {
        this.setState({ dialogShow: false })
    }

    render() {
        console.log("quant list", this.state.quantityList)

        //const { navigation } = this.props;  
        //const newOrders = navigation.getParam('newOrders');  
        return (
            <View style={[styles.container]}>
                <Dialog
                    dialogStyle={{ borderRadius: 25, flex: 1, position: 'absolute', top: 120 }}
                    visible={this.state.dialogShow}
                    width={0.8}
                    height={0.7}
                    animationDuration={20000}
                    onHardwareBackPress={() => false}
                    onTouchOutside={() => {
                        this.setState({ visible: false });
                    }}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}>
                    <DialogContent style={[styles.dialogContainer, { flex: 1 }]}>
                        <TouchableOpacity onPress={this.closePopUp.bind(this)} style={styles.iconStyle}>
                            <Image source={Constants.Images.closeDialog} style={{ height: 40, width: 40 }} />
                        </TouchableOpacity>
                        <Text style={{ margin: 20, fontSize: 24, color: Constants.Colors.White, alignSelf: 'center', textAlign: 'center', justifyContent: 'center' }}>Select Driver</Text>
                        {this.state.deliveryBoyList && this.state.deliveryBoyList.length > 0 && this.state.deliveryBoyList.map((data, key) => {
                            return (
                                <TouchableOpacity onPress={() => this.driverSelected(data)} key={key} style={[styles.productBox, { flexDirection: 'row' }]}>
                                    <View style={{ flex: .7, flexDirection: 'column', justifyContent: 'space-between',}}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{'Name: '+data.name}</Text>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{'Vehicle: '+data.vehicle_type}</Text>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{'Vehicle No.: '+data.vehicle_number}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}

                    </DialogContent>
                </Dialog>
                <Background style={styles.container}>

                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>

                        <KeyboardAvoidingView behavior={'position'} style={{ margin: 20 }}>

                            {/* <Text style={{ marginTop: 25, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Serial No.:
                        </Text> */}
                            <Text style={{ marginTop: 10, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Order Id: {this.state.newOrders.order_id}
                            </Text>
                            <Text style={{ marginTop: 10, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Customer Name: {this.state.newOrders.customer_name}
                            </Text>
                            <Text style={{ marginTop: 10, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Delivery Address: {this.state.newOrders.delivery_address}
                            </Text>
                            {/* <Text style={{ marginTop: 10, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Delivery Partner/Boy Contact No.:
                        </Text> */}

                            {this.state.newOrders && this.state.newOrders.products && this.state.newOrders.products.length > 0 && this.state.newOrders.products.map((data, key) => {
                                return (
                                    <View key={key} style={[styles.productBox, { flexDirection: 'row', flex: 1 }]}>
                                        <View style={{ flex: .7, flexDirection: 'row', justifyContent: 'space-between', }}>
                                            <Text style={{ flex: .8, fontSize: 18, color: Constants.Colors.White }}>{data.product_name}</Text>
                                            <Text style={{ flex: .2, fontSize: 18, color: Constants.Colors.White, textAlign: 'center' }}>{'₹'+data.total_price}</Text>
                                        </View>
                                        <View style={{ flex: .3, flexDirection: 'row', justifyContent: 'center' }}>
                                            <TouchableOpacity onPress={() => data.quantity > 1 ? this.sub(data, key) : null} style={{ fontSize: 18, color: Constants.Colors.White }}>
                                                <Text style={[styles.productBoxQuantity]}>{'-'}</Text>
                                            </TouchableOpacity>
                                            <Text style={{ alignSelf: 'center', textAlign: 'center', marginLeft: 10, marginRight: 10, fontSize: 18, color: Constants.Colors.White }}>{data.quantity}</Text>
                                            <TouchableOpacity onPress={() => data.quantity < this.state.quantityList[key] ? this.add(data, key) : null} style={{ fontSize: 18, color: Constants.Colors.White }}>
                                                <Text style={[styles.productBoxQuantity]}>{'+'}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            })}

                            <View style={[styles.rowContainerStyle, { margin: 10 }]}>
                                {this.renderBottomButtons()}

                                <TouchableOpacity onPress={() => this.saveAction("0")} style={[styles.goToSignInOuter, { backgroundColor: Constants.Colors.Orange }]}>
                                    <Text style={[styles.goToSignInInner]}>Decline All</Text>
                                </TouchableOpacity>
                            </View>

                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Bill: ₹{this.state.newOrders.bill}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Total Bill: ₹{this.state.newOrders.total_bill}
                            </Text>
                            {/* <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Delivery Status: {this.state.newOrders.status}
                            </Text> */}
                        </KeyboardAvoidingView>
                    </ScrollView>
                </Background>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    dialogContainer: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        left: 0,
        backgroundColor: Constants.Colors.LightBlue
    },
    iconStyle: {
        alignSelf: 'flex-end',
        position: 'absolute',
        top: 2,
        right: 2,
        height: 40,
        width: 40
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        //height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    productBoxQuantity: {
        alignSelf: "center",
        borderRadius: 5,
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})