import React, { Component } from 'react'
import { Animated, TextInput, Dimensions, Image, Switch, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';
import { Dropdown } from 'react-native-material-dropdown';

const { width } = Dimensions.get('window')

export default class ManageProducts extends Component {
    constructor(props) {
        super()
        this.state = {
            vendorProducts: [],
            categoriesList: [],
            tokenValue: '',
            searchText: '',
            categorySearch: ''
        }
        this.arrayholder = [];
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getvendorProducts();
    }

    getvendorProducts() {
        let categoryListData = []
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/products", {}, tokenValue).then((result) => {
                console.log("vendor new:", JSON.stringify(result));
                if (result.success == "1") {
                    result.Vendor_Products && result.Vendor_Products.map((item, key) => {
                        categoryListData.push({ value: item.category })
                    })
                    this.setState({ vendorProducts: result.Vendor_Products, categoriesList: categoryListData })
                    this.arrayholder = result.Vendor_Products;
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    searchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.product_name ? item.product_name.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            vendorProducts: newData,
            searchText: text,
        });
    }

    searchCategoryFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.category ? item.category.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            vendorProducts: newData,
            categorySearch: text,
        });
    }

    checkProductAvailability(productId, status) {
        RestClient.putQuery("api/vendor/products/status/update", { product_id: productId, status: status ? 1 : 0 }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                this.getvendorProducts();
                //alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        let data = [{
            value: 'Banana',
        }, {
            value: 'Mango',
        }, {
            value: 'Pear',
        }];
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 10 }}>

                        <TextInput
                            style={styles.textInputStyle}
                            onChangeText={text => this.searchFilterFunction(text)}
                            value={this.state.text}
                            underlineColorAndroid="transparent"
                            placeholder="Search Here"
                            placeholderTextColor={Constants.Colors.Black}
                        />

                        <Dropdown
                            value={this.state.categorySearch}
                            textColor={Constants.Colors.White}
                            data={this.state.categoriesList}
                            label='Select Category'
                            itemColor={Constants.Colors.Black}
                            selectedItemColor={Constants.Colors.Black}
                            baseColor={Constants.Colors.White}
                            //itemTextStyle={{ color: Constants.Colors.Black }}
                            onChangeText={text => this.searchCategoryFunction(text)}
                        />

                        {this.state.vendorProducts && this.state.vendorProducts.length > 0 && this.state.vendorProducts.map((data, key) => {
                            return (
                                <View key={key} style={[styles.productBox, { margin: 5, flexDirection: 'row', justifyContent: 'space-between', flex: 1 }]}>
                                    <Text style={{flex: .8, fontSize: 18, color: Constants.Colors.White }}>{data.product_name}</Text>
                                    <Switch
                                        value={!data.available}
                                        onValueChange={(switchValue) => this.checkProductAvailability(data.product_id, data.available)} />
                                </View>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        //height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
    textInputStyle: {
        height: 40,
        marginBottom: 10,
        borderWidth: 1,
        paddingLeft: 10,
        borderColor: '#009688',
        backgroundColor: '#FFFFFF',
    },
})