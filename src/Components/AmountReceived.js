import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';
import { Dropdown } from 'react-native-material-dropdown';

const { width } = Dimensions.get('window')
const months = ["January", "February", "March", "April",
    "May", "June", "July", "August", "September", "October",
    "November", "December"];

export default class AmountReceived extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            yearMonthSales: [],
            selectedYear: new Date().getFullYear(),
            allYears: []
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getYearsList();
        this.getNewDetails(this.state.selectedYear);
    }

    getYearsList(){
        let yearList = []
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/order/yearslist", {}, tokenValue).then((result) => {
                console.log("year response:", JSON.stringify(result));
                if (result.success == "1") {
                    result.all_years && result.all_years.map((item, key) => {
                        yearList.push({ value: item })
                    })
                    this.setState({ allYears: yearList })
                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    getNewDetails(year) {
        this.setState({ selectedYear: year })
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/amount/months_list", { year: year }, tokenValue).then((result) => {
                console.log("vendor new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ yearMonthSales: result.month_amount_list })
                } else {
                    this.setState({ monthsList: [] })
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 20 }}>

                        <Dropdown
                            value={this.state.selectedYear}
                            containerStyle={[styles.textInputStyle]}
                            textColor={Constants.Colors.White}
                            data={this.state.allYears}
                            label='Select Year'
                            itemColor={Constants.Colors.Black}
                            selectedItemColor={Constants.Colors.Black}
                            baseColor={Constants.Colors.White}
                            itemTextStyle={{ color: Constants.Colors.Black }}
                            onChangeText={year => this.getNewDetails(year)}
                        />

                        {this.state.yearMonthSales && this.state.yearMonthSales.length > 0 && this.state.yearMonthSales.map((data, key) => {
                            return (
                                <TouchableOpacity onPress={() => navigate('MonthTableChart', {year: this.state.selectedYear, month: data.month})} key={key} style={[styles.productBox, { margin: 5, flexDirection: 'row', justifyContent: 'space-between', flex: 1 }]}>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{months[data.month]}</Text>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.White }}>{data.sales}</Text>
                                </TouchableOpacity>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
    textInputStyle: {
        color: Constants.Colors.Black
    },
})