'use strict';
import { Platform } from 'react-native';
import FCM, {FCMEvent} from 'react-native-fcm';
import _ from "lodash";
let refreshTokenListener;
let notificationListener;
import Constants from "../Utilities/Constants";

export function pushNotificationInit(store) {
    FCM.requestPermissions({ badge: false, sound: true, alert: true }); // for iOS
    // FCM token on intial app load.
    FCM.getFCMToken().then(token => {
        if (token) {
            //scheduleNotifications({ date: new Date() })
            console.log("device token fcm1", token)
            Constants.registrationId = token;

            //alert(token)
            // here we get the device token
            //store.dispatch(userActions.setDeviceTokenFCM(token));
        }
    });

    // Receive Notification in kill state, inactive state or bankground state.
    FCM.getInitialNotification().then(res => {
        //alert("Notification background")
        let context = this;
        if (JSON.stringify(res)) {
            setTimeout(function () {
                //onNotificationRedirection(res, store);
            }, 500);
        }
    });

    FCM.createNotificationChannel({
        id: 'my_default_channel',
        name: 'Car status',
        description: 'Notifies when changes car status',
        priority: 'max',
    });

    //Receive Notification in forground
    notificationListener = FCM.on(FCMEvent.Notification, async (res) => {
        console.log('forground', res)
        setTimeout(function () {
            scheduleNotifications(res)
        }, 500);
    });

    // Fcm token may not be available on first load, catch it here
    refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        if (token) {
            //console.log("device token fcm2", token)
            // token here
            //store.dispatch(userActions.setDeviceTokenFCM(token));
        }
    });
}

/**
* Schedule Local Notifications.
*/
export function scheduleNotifications(data) {
    if (Platform.OS == 'android') {
        FCM.presentLocalNotification({
            channel: 'my_default_channel',
            id: new Date().valueOf().toString(),         // (optional for instant notification)
            title: data.fcm.title,      // as FCM payload
            body: data.fcm.body,                // as FCM payload (required)
            priority: "high",
            show_in_foreground: true,                    // notification when app is in foreground (local & remote)
            //vibrate: 1000,
            local: true,
            icon: "logo",
            sound: 'default',
            lights: true
        });
    } else {
        FCM.presentLocalNotification({
            channel: 'my_default_channel',
            id: new Date().valueOf().toString(),         // (optional for instant notification)
            title: data.notification.title,      // as FCM payload
            body: data.notification.body,                // as FCM payload (required)
            priority: "high",
            show_in_foreground: true,                    // notification when app is in foreground (local & remote)
            //vibrate: 1000,
            local: true,
            icon: "logo",
            sound: 'default',
        });
    }
};

/**
* Get Scheduled Notifications List.
*/
export function getScheduleNotifications(data, callback) {
    FCM.getScheduledLocalNotifications().then(notification => {
        if (_.isFunction(callback)) {
            callback(notification);
        }
    });
};

/**
*  Removes all future local notifications.
*/
export function cancelAllLocalNotifications() {
    FCM.cancelAllLocalNotifications();
};

/**
* Stop listening push notification events
*/
export function pushNotificationRemove(store) {
    refreshTokenListener.remove();
}