import React, { Component } from 'react'
import { TextInput, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';
import { scaleHeight, scaleWidth, normalizeFont } from "../Utilities/Responsive";
import Slider from '@react-native-community/slider'

const { width } = Dimensions.get('window')

export default class AdjustDeliveryOption extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            email: '',
            contact: '',
            outletName: '',
            outletAddress: '',
            tokenValue: '',
            //Initial Value of slider
            sliderValue: 40,
            deliveryFee: "0",
            deliveryTime: "0"
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getDeliveryDetails();
    }

    getDeliveryDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/vendor/delivery_details", {}, tokenValue).then((result) => {
                console.log("delivery_details:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ sliderValue: result.delivery_details.delivery_range == 'None' ? 0 : Number(result.delivery_details.delivery_range), deliveryFee: result.delivery_details.delivery_fee == 'None' ? '0' : result.delivery_details.delivery_fee, deliveryTime: result.delivery_details.delivery_time == null ? '0' : result.delivery_details.delivery_time })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    saveDeliveryDetails() {
        RestClient.put("api/vendor/delivery_details/update", { delivery_range: this.state.sliderValue, delivery_fee: this.state.deliveryFee, delivery_time: this.state.deliveryTime }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 20 }}>
                        <View style={styles.logo} />

                        <Text style={{ alignSelf: 'center', marginTop: 20, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }}>
                            Adjust range for delivery
                        </Text>
                        <Text>Range : {this.state.sliderValue}</Text>
                        {/*Slider with max, min, step and initial value*/}
                        <Slider
                            maximumValue={200}
                            minimumValue={0}
                            minimumTrackTintColor="#307ecc"
                            maximumTrackTintColor="#000000"
                            step={1}
                            value={this.state.sliderValue}
                            onValueChange={(sliderValue) => this.setState({ sliderValue })}
                        />

                        <Text style={{ alignSelf: 'center', marginTop: 20, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }}>
                            Delivery Fee
                        </Text>
                        <Text style={{ color: Constants.Colors.White, marginBottom: 15, fontSize: 18 }}>
                            Current Delivery fee: Rs {this.state.deliveryFee}
                        </Text>
                        <View style={{ flexDirection: 'row', marginBottom: 15, justifyContent: 'space-between', flex: 1, alignItems: 'center' }}>
                            <Text style={{ color: Constants.Colors.White, fontSize: 18 }}>
                                Change Delivery fee:
                            </Text>
                            <TextInput
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                maxLength={4}
                                keyboardType="numeric"
                                returnKeyType='done'
                                onChangeText={(deliveryFee) => this.setState({ deliveryFee: deliveryFee })}
                                value={this.state.deliveryFee} />

                        </View>

                        <Text style={{ alignSelf: 'center', marginTop: 20, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }}>
                            Delivery Time
                        </Text>
                        <Text style={{ color: Constants.Colors.White, marginBottom: 15, fontSize: 18 }}>
                            Current Delivery Time:  {this.state.deliveryTime}min
                        </Text>
                        <View style={{ flexDirection: 'row', marginBottom: 15, justifyContent: 'space-between', flex: 1, alignItems: 'center' }}>
                            <Text style={{ color: Constants.Colors.White, fontSize: 18 }}>
                                Change Delivery Time:
                        </Text>
                            <TextInput
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                maxLength={3}
                                keyboardType="numeric"
                                returnKeyType='done'
                                onChangeText={(deliveryTime) => this.setState({ deliveryTime: deliveryTime })}
                                value={this.state.deliveryTime} />
                        </View>

                        <SubmitButton
                            onPress={() => this.saveDeliveryDetails()}
                            text="Submit"
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    input: {
        height: scaleHeight(42),
        width: scaleWidth(70),
        backgroundColor: '#FFFFFF',
        fontSize: normalizeFont(16),
        borderRadius: scaleWidth(5),
        alignSelf: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    },
})