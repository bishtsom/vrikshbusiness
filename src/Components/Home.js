import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions, StackActions } from "react-navigation"

const { width } = Dimensions.get('window')
const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Login' })
    ], key: null
});
export default class Home extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            email: '',
            contact: '',
            outletName: '',
            outletAddress: ''
        }
    }

    logOut() {
        AsyncStorage.clear();
        this.props.navigation.dispatch(resetActionLogin)
    }

    render() {
        const {navigate} = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>
                        {/* <View style={styles.logo} /> */}

                        <SubmitButton
                            onPress={() => navigate("MyProfile")}
                            text="MyProfile"
                        />
                        <SubmitButton
                            onPress={() => navigate("AdjustDeliveryOption")}
                            text="Adjust Delivery Options"
                        />
                        <SubmitButton
                            onPress={() => navigate("ManageProducts")}
                            text="Manage Products"
                        />
                        <SubmitButton
                            onPress={() => navigate("ManageOrders")}
                            text="Manage Orders"
                        />
                        <SubmitButton
                            onPress={() => navigate("SalesAndFigures")}
                            text="Sales & Figures"
                        />
                        <SubmitButton
                            onPress={() => navigate("AddNewDriver")}
                            text="Add New Driver"
                        />
                        <SubmitButton
                            onPress={() => navigate("Complaints")}
                            text="Complaints"
                        />
                        <SubmitButton
                            onPress={() => this.logOut()}
                            text="Logout"
                        />
                        <View style={{height: 10}}></View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
})